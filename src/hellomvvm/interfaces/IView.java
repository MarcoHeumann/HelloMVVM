package hellomvvm.interfaces;

import hellomvvm.views.RootViewModel;
import javafx.beans.property.StringProperty;

/**
 * Basic Interface
 * Not used much for this tutorial.
 * Only here to show where it would fit in and how you could use it in your code
 */
public interface IView
{
	public void setParent(RootViewModel p);

	public StringProperty getStrPropGlobal();
}
