package hellomvvm.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Message Objekt.
 * This will be used for on screen text binding and possibly for two way databinding as well.
 */
public class Message
{
	/**
	 * StringProperty > String due to build in databinding and syncing.
	 */
	private StringProperty message = new SimpleStringProperty();

	/**
	 * Constructor, duh!
	 */
	public Message(String s)
	{
		this.message.set(s);
	}

	public StringProperty getMessage()
	{
		return message;
	}

	public void setMessage(StringProperty message)
	{
		this.message = message;
	}
}
