package hellomvvm.views;

import hellomvvm.interfaces.IView;
import hellomvvm.models.Message;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * ViewModel for the first View, implements IView
 * Includes all the logic needed by the View itself.
 * 
 * @author Marco
 *
 */
public class FirstViewModel implements IView
{
	@FXML
	private Button			btnToSecond;

	@FXML
	private TextField		tfGlobal, tfLocal;

	@FXML
	private Text			txtGlobal, txtLocal;

	private RootViewModel	rootView;
	private Message			msgLocal;
	private StringProperty	strPropGlobal	= new SimpleStringProperty();

	/**
	 * Called automatically once all the FX components have been created and placed.
	 * Perfect spot to set some values as you can be sure everything is already there (initialized).
	 */
	public void initialize()
	{
		// create a local message to display
		msgLocal = new Message("Local ONE. All personal stuff!");

		// databind the global message from parent

		tfGlobal.textProperty().bindBidirectional(strPropGlobal);
		txtGlobal.textProperty().bind(strPropGlobal);

		// databind the local message
		tfLocal.textProperty().bindBidirectional(msgLocal.getMessage());
		txtLocal.textProperty().bind(msgLocal.getMessage());
	}

	/**
	 * Called on click of the button. Asks the parent to change subViews
	 */
	@FXML
	private void switchView()
	{
		rootView.requestViewChange("SecondView");
	}

	@Override
	public void setParent(RootViewModel p)
	{
		this.rootView = p;
	}

	@Override
	public StringProperty getStrPropGlobal()
	{
		return strPropGlobal;
	}
}
