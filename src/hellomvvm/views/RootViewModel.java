package hellomvvm.views;

import java.io.IOException;
import java.util.HashMap;

import hellomvvm.interfaces.IView;
import hellomvvm.models.Message;
import hellomvvm.settings.GeneralSettings;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * ViewModel for the window root.
 * This is usually used for all the overarching logic like changing sub views,
 * storing global values that need to be accessed by multiple views ect.
 */
public class RootViewModel
{
	/**
	 * Used for manipulating the basic window, like the title. Not used in this tutorial.
	 */
	public Stage						pStage;
	private AnchorPane					rootView;
	/**
	 * Stores the views so you don't have to recreate them each time and can instead check if they are already there.
	 */
	private HashMap<String, GridPane>	myViews		= new HashMap<String, GridPane>();
	private Message						msgGlobal	= new Message("Global Binding Magic!");

	/**
	 * Constructor that takes a Stage object from the start file.
	 */
	public RootViewModel(Stage s)
	{
		pStage = s;
		createWindow();
		switchToView("FirstView");
	}

	/**
	 * Creates the basic window via some settings defined elsewhere.
	 */
	private void createWindow()
	{
		try
		{
			// loading of an FXML File is done like this
			rootView = new FXMLLoader().load(getClass().getResourceAsStream("RootView.fxml"));

			// setting the root scene (you can use values for width and height, or just omit them. The included values in the fxml files will be used then.
			Scene scene = new Scene(rootView, GeneralSettings.WINDOW_WIDTH, GeneralSettings.WINDOW_HEIGHT);

			// set a style sheet to make stuff look pretty like a princess (not a Shrek like one)
			scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());

			// set the stuff onto the primary stage and get the title as well
			pStage.setScene(scene);
			pStage.setTitle(GeneralSettings.WINDOW_TITLE);
			pStage.show();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Used to switch to a different subView.
	 * Checks if a view is already created and possibly reuses it.
	 * 
	 * @param view View to be loaded.
	 */
	private void switchToView(String view)
	{
		// if the view is not yet saved in myViews, create a new one, else just reuse it
		if (!myViews.containsKey(view))
		{
			// the loader is used to get the controller of the subView, so this reference is needed
			FXMLLoader loader = new FXMLLoader();
			try
			{
				GridPane subView = loader.load(getClass().getResourceAsStream(view + ".fxml"));
				// IMPORTANT LINE: call this method on the new View to pass it the parent "this" and any global bindings. The Interface helps to make this work easily.
				setGlobalBindings(loader.<IView>getController());
				myViews.put(view, subView);
			}
			catch (IOException e)
			{
			}
		}

		// now add the subView to rootView
		// only remove the Nodes saved in myViews. This leaves potential nodes that are part of the root itself. Like a menu on top.
		rootView.getChildren().removeAll(myViews.values());
		rootView.getChildren().add(myViews.get(view));
	}

	/**
	 * Passes stuff to the children for later use.
	 * References, Bindings, whatever.
	 * 
	 * @param iv
	 */
	private void setGlobalBindings(IView iv)
	{
		iv.setParent(this);
		iv.getStrPropGlobal().bindBidirectional(msgGlobal.getMessage());
	}

	/**
	 * public implementation of switchToView.
	 * In theory this would double check the request and act accordingly.
	 * 
	 * Use this like a setter kind of deal.
	 * 
	 * @param view
	 */
	public void requestViewChange(String view)
	{
		// check if the request is fine. It most likely is, so...
		switchToView(view);
	}
}
