package hellomvvm;

import hellomvvm.views.RootViewModel;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Entry point for every application.
 * You should never put logic in a big manner in here.
 * Use this to setup stuff like global loggers and load the root window with its controller.
 */
public class HelloMVVM extends Application
{
	@Override
	public void start(Stage primaryStage)
	{
		setupAllTheThings();
		new RootViewModel(primaryStage);
	}

	/**
	 * Place to set up global stuff.
	 * Loading of settings, adding a global logger, getting logger settings, ect.
	 */
	private void setupAllTheThings()
	{
		// nothing here to see
	}

	/**
	 * Keep this as it is. No need to change anything from the default you get when creating a new FX Project.
	 */
	public static void main(String[] args)
	{
		launch(args);
	}
}
