package hellomvvm.settings;

/*
 * Used for any kind of static settings.
 * Can be replaced by an external settings file, like a .xml-file for example.
 */
public abstract class GeneralSettings
{
	public final static int		WINDOW_WIDTH	= 500;
	public final static int		WINDOW_HEIGHT	= 500;
	public final static String	WINDOW_TITLE	= "Hello MVVM World!";
}
